﻿Feature: Create Phone
	As a candidate evaluator
	I want to add a phone to the system
	So that I can verify Diego knows what's doing when running write operations

@phone
Scenario: Adds a phone
	Given there are no phones in the system
	And there is a phone
	And its brand is "1"
	And its model is "NG400"
	And its description is "The best budget phone"
	When I add the phone
	Then a phone is created in the system
