﻿using Phones.BackEnd.Domain.Contracts;
using Phones.BackEnd.Domain.Exceptions;
using Phones.BackEnd.Types;
using System;

namespace Phones.BackEnd.Domain
{
    public class PhoneAggregate
        : IAggregate
    {
        public Guid Id { get; }
        public BrandType Brand { get; }
        public string Model { get; }
        public string Description { get; }
        public Uri ImageUri { get; private set; }

        public PhoneAggregate(
            Guid id,
            BrandType brand,
            string model,
            string description)
        {
            Id = id;
            Brand = brand;
            Model = model ?? throw new ArgumentNullException(nameof(model));
            Description = description;
            ImageUri = null;
        }

        public void AddImageUri(Uri imageUri)
        {
            if (ImageUri != null)
            {
                throw new ImageAlreadyExistsException($"There is already an image at {ImageUri}");
            }

            ImageUri = imageUri;
        }
    }
}
