﻿using FluentAssertions;
using Phones.BackEnd.Domain.Exceptions;
using System;
using ToolBelt.TestSupport;
using Xunit;

namespace Phones.BackEnd.Domain.UnitTests.Exceptions.ImageAlreadyExistsExceptionTests
{
    public static class ConstructorTests
    {
        public class Given_A_Message_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private ImageAlreadyExistsException _sut;
            private string _message;

            protected override void Given()
            {
                _message = "foo";
            }

            protected override void When()
            {
                _sut = new ImageAlreadyExistsException(_message);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Have_The_Correct_Message()
            {
                _sut.Message.Should().Be(_message);
            }

            [Fact]
            public void Then_It_Should_Not_Have_Any_InnerException()
            {
                _sut.InnerException.Should().BeNull();
            }
        }

        public class Given_A_Message_And_An_InnerException_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private ImageAlreadyExistsException _sut;
            private string _message;
            private Exception _exception;

            protected override void Given()
            {
                _message = "foo";
                _exception = new Exception();
            }

            protected override void When()
            {
                _sut = new ImageAlreadyExistsException(_message, _exception);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Have_The_Correct_Message()
            {
                _sut.Message.Should().Be(_message);
            }

            [Fact]
            public void Then_It_Should_Have_The_Correct_InnerException()
            {
                _sut.InnerException.Should().Be(_exception);
            }
        }
    }
}