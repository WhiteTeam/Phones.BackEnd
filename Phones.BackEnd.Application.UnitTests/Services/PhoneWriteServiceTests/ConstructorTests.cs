﻿using FluentAssertions;
using Moq;
using Phones.BackEnd.Application.Contracts.Factories;
using Phones.BackEnd.Application.Contracts.Repositories;
using Phones.BackEnd.Application.Contracts.Services;
using Phones.BackEnd.Application.Services;
using Phones.BackEnd.Domain;
using ToolBelt.TestSupport;
using Xunit;

namespace Phones.BackEnd.Application.UnitTests.Services.PhoneWriteServiceTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private PhoneWriteService _sut;
            private IRepository<PhoneAggregate> _repository;
            private IIdFactory _idFactory;

            protected override void Given()
            {
                _repository = Mock.Of<IRepository<PhoneAggregate>>();
                _idFactory = Mock.Of<IIdFactory>();
            }

            protected override void When()
            {
                _sut = new PhoneWriteService(_repository, _idFactory);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IPhoneWriteRepository()
            {
                _sut.Should().BeAssignableTo<IPhoneWriteService>();
            }
        }
    }
}