﻿namespace Phones.BackEnd.Types
{
    public enum BrandType
    {
        None = 0,
        Motorola = 1,
        Nokia = 2
    }
}