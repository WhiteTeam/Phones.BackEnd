﻿using Microsoft.EntityFrameworkCore;
using Phones.BackEnd.Application.Contracts;
using Phones.BackEnd.Application.Contracts.Repositories;
using Phones.BackEnd.Domain;
using Phones.BackEnd.Repository.Infra.EntityFramework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phones.BackEnd.Repository.Infra.EntityFramework
{
    public class EntityFrameworkRepository
        : IRepository<PhoneAggregate>
    {
        private readonly PhoneContext _phoneContext;
        private readonly IMapper<PhoneAggregate, PhoneEntity> _phoneEntityMapper;
        private readonly IMapper<PhoneEntity, PhoneAggregate> _phoneAggregateMapper;

        public EntityFrameworkRepository(
            PhoneContext phoneContext,
            IMapper<PhoneAggregate, PhoneEntity> phoneEntityMapper,
            IMapper<PhoneEntity, PhoneAggregate> phoneAggregateMapper)
        {
            _phoneContext = phoneContext;
            _phoneEntityMapper = phoneEntityMapper;
            _phoneAggregateMapper = phoneAggregateMapper;
        }

        public async Task Persist(PhoneAggregate aggregate)
        {
            var id = aggregate.Id;
            var phoneEntity = _phoneEntityMapper.Map(aggregate);
            var existingPhoneEntity = await _phoneContext.Phones.Where(x => x.Id == id).SingleOrDefaultAsync();
            if (existingPhoneEntity != null)
            {
                _phoneContext.Phones.Remove(existingPhoneEntity);
                await _phoneContext.Phones.AddAsync(phoneEntity);
            }
            await _phoneContext.Phones.AddAsync(phoneEntity);
            await _phoneContext.SaveChangesAsync();
        }

        public async Task<PhoneAggregate> Get(Guid id)
        {
            var existingPhoneEntity = await _phoneContext.Phones.Where(x => x.Id == id).SingleOrDefaultAsync();
            var phoneAggregate = _phoneAggregateMapper.Map(existingPhoneEntity);
            return phoneAggregate;
        }

        public async Task<IEnumerable<PhoneAggregate>> GetAll()
        {
            var existingPhoneEntities = await _phoneContext.Phones.ToListAsync();
            var phoneAggregates = existingPhoneEntities.Select(x => _phoneAggregateMapper.Map(x));
            return phoneAggregates;
        }
    }
}
