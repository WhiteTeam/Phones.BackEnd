﻿using Microsoft.AspNetCore.Mvc;
using Phones.BackEnd.Application.Contracts.Services;
using Phones.BackEnd.Application.Model;
using System;
using System.Threading.Tasks;

namespace Phones.BackEnd.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class PhonesController
        : Controller
    {
        private readonly IPhoneWriteService _phoneWriteService;
        private readonly IPhoneReadService _phoneReadService;

        public PhonesController(
            IPhoneWriteService phoneWriteService,
            IPhoneReadService phoneReadService)
        {
            _phoneWriteService = phoneWriteService;
            _phoneReadService = phoneReadService;
        }

        [HttpPost("")]
        public async Task<IActionResult> CreatePhone([FromBody]PhoneNew phone)
        {
            await _phoneWriteService.CreatePhone(phone);
            var newId = phone.Id;
            var routeValues = new { id = newId };
            var response = CreatedAtRoute("get.phone", routeValues, phone);
            return response;
        }

        [HttpGet("{id}", Name = "get.phone")]
        public async Task<IActionResult> GetPhone(Guid id)
        {
            var phone = await _phoneReadService.GetPhone(id);
            return Ok(phone);
        }

        [HttpGet("")]
        public async Task<IActionResult> GetPhones()
        {
            var phones = await _phoneReadService.GetPhones();
            return Ok(phones);
        }

        [HttpPut("{id}/image")]
        [HttpPatch("{id}/image")]
        public async Task<IActionResult> UpdateImage(Guid id, [FromBody] Image image)
        {
            await _phoneWriteService.AddImage(id, image.Uri);
            return Ok();
        }
    }
}
