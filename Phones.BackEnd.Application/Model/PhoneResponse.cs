﻿using System;

namespace Phones.BackEnd.Application.Model
{
    public class PhoneResponse
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Uri ImageUri { get; set; }
    }
}