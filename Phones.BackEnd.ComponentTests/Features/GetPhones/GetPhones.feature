﻿Feature: Get Phones
	As a candidate evaluator
	I want to view the list of phones in the system
	So that I can verify Diego knows what's doing when running read operations

@phone
Scenario: Get phones
	Given there are 3 phones in the system
	When I get the phones
	Then a list of 3 phones is displayed
