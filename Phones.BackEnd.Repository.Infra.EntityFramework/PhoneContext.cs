﻿using Microsoft.EntityFrameworkCore;
using Phones.BackEnd.Repository.Infra.EntityFramework.Entities;

namespace Phones.BackEnd.Repository.Infra.EntityFramework
{
    public class PhoneContext
        : DbContext
    {
        public PhoneContext(DbContextOptions<PhoneContext> options)
            : base(options)
        {
        }

        public DbSet<PhoneEntity> Phones { get; set; }
    }
}
