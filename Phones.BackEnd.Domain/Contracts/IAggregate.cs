﻿using System;

namespace Phones.BackEnd.Domain.Contracts
{
    public interface IAggregate
    {
        Guid Id { get; }
    }
}