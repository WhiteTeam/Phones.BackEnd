﻿using FluentAssertions;
using Phones.BackEnd.Application.Factories;
using System;
using ToolBelt.TestSupport;
using Xunit;

namespace Phones.BackEnd.Application.UnitTests.Factories.IdFactoryTests
{
    public static class CreateTests
    {
        public class Given_The_Factory_When_Creating
            : Given_When_Then_Test
        {
            private IdFactory _sut;
            private Guid _result;

            protected override void Given()
            {
                _sut = new IdFactory();
            }

            protected override void When()
            {
                _result = _sut.Create();
            }

            [Fact]
            public void Then_It_Should_Not_Be_Empty_Guid()
            {
                _result.Should().NotBe(Guid.Empty);
            }
        }
    }
}