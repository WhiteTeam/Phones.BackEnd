﻿using System;
using FluentAssertions;
using Phones.BackEnd.Types;
using ToolBelt.TestSupport;
using Xunit;

namespace Phones.BackEnd.Domain.UnitTests.Model.PhoneAggregateTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private PhoneAggregate _sut;
            private Guid _id;
            private BrandType _brand;
            private string _model;
            private string _description;

            protected override void Given()
            {
                _id = GuidGenerator.Create(1);
                _brand = BrandType.Motorola;
                _model = "foo";
                _description = "bar";
            }

            protected override void When()
            {
                _sut = new PhoneAggregate(_id, _brand, _model, _description);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Have_The_Correct_Id()
            {
                _sut.Id.Should().Be(_id);
            }

            [Fact]
            public void Then_It_Should_Have_The_Correct_Brand()
            {
                _sut.Brand.Should().Be(_brand);
            }

            [Fact]
            public void Then_It_Should_Have_The_Correct_Model()
            {
                _sut.Model.Should().Be(_model);
            }

            [Fact]
            public void Then_It_Should_Have_The_correct_Description()
            {
                _sut.Description.Should().Be(_description);
            }

            [Fact]
            public void Then_It_Should_Not_Have_An_Image()
            {
                _sut.ImageUri.Should().BeNull();
            }
        }

        public class Given_Invalid_Model_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private PhoneAggregate _sut;
            private Guid _id;
            private BrandType _brand;
            private string _model;
            private string _description;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _id = GuidGenerator.Create(1);
                _brand = BrandType.Motorola;
                _model = null;
                _description = "bar";
            }

            protected override void When()
            {
                try
                {
                    _sut = new PhoneAggregate(_id, _brand, _model, _description);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Not_Have_Created_A_Valid_Instance()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }
    }
}