﻿using System;

namespace Phones.BackEnd.Domain.Exceptions
{
    public class ImageAlreadyExistsException
        : Exception
    {
        public ImageAlreadyExistsException(string message)
            : base(message)
        {
        }

        public ImageAlreadyExistsException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}
