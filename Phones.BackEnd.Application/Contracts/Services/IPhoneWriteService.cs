﻿using Phones.BackEnd.Application.Model;
using System;
using System.Threading.Tasks;

namespace Phones.BackEnd.Application.Contracts.Services
{
    public interface IPhoneWriteService
    {
        Task CreatePhone(PhoneNew phone);
        Task AddImage(Guid phoneId, Uri imageUri);
    }
}